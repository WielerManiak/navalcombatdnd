import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Hull } from '../_models/hull';
import { HullService } from '../_services/hull.service';

@Component({
  selector: 'app-hull-edit',
  templateUrl: './hull-edit.component.html',
  styleUrls: ['./hull-edit.component.scss'],
})
export class HullEditComponent implements OnInit {
  hullForm: FormGroup;
  hull: Hull;
  id: number;
  isCreateMode: boolean;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private hullService: HullService
  ) {}

  ngOnInit(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.isCreateMode = !this.id;

    if (this.isCreateMode) {
      this.hull = new Hull(undefined, '', undefined, undefined, undefined);

      this.hullForm = new FormGroup({
        name: new FormControl('', [Validators.required]),
        AC: new FormControl('', [Validators.required]),
        hitpoints: new FormControl('', [Validators.required]),
        damageTreshold: new FormControl('', [Validators.required]),
      });
    }

    if(!this.isCreateMode){
      this.getHull(this.id);
    }
  }

  get f() {
    return this.hullForm.controls;
  }

  getHull(id: number){
    this.hullService.getHull(id).subscribe((hull) =>{
      this.hull = hull;

      this.hullForm = new FormGroup({
        name: new FormControl(this.hull.name, [Validators.required]),
        AC: new FormControl(this.hull.AC, [Validators.required]),
        hitpoints: new FormControl(this.hull.hitpoints, [Validators.required]),
        damageTreshold: new FormControl(this.hull.damageTreshold, [Validators.required]),
      });
    })
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;

    if (this.hullForm.invalid) {
      return;
    }

    if (this.isCreateMode) {
      this.createHull();
    }else{
      this.updateHull();
    }
  }

  private createHull() {
    this.hull = this.hullForm.value;

    this.hullService.addHull(this.hull).subscribe(() => {
      this.goBack();
      this.loading = false;
    });
  }

  private updateHull() {
    this.hull.name = this.hullForm.value.name;
    this.hull.AC = this.hullForm.value.AC;
    this.hull.hitpoints = this.hullForm.value.hitpoints;
    this.hull.damageTreshold = this.hullForm.value.damageTreshold;

    this.hullService.updateHull(this.hull).subscribe(() => {
      this.goBack();
      this.loading = false;
    })
  }

  goBack(): void {
    this.router.navigate(['/hull/list']);
  }
}
