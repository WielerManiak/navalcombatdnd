import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShipComponent } from './ship/ship.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TeamsComponent } from './teams/teams.component';
import { HullListComponent } from './hull-list/hull-list.component';
import { HullEditComponent } from './hull-edit/hull-edit.component';
import { HelmListComponent } from './helm-list/helm-list.component';
import { HelmEditComponent } from './helm-edit/helm-edit.component';
import { SailsListComponent } from './sails-list/sails-list.component';
import { SailsEditComponent } from './sails-edit/sails-edit.component';
import { ShipListComponent } from './ship-list/ship-list.component';
import { ShipEditComponent } from './ship-edit/ship-edit.component';
import { CannonListComponent } from './cannon-list/cannon-list.component';
import { CannonEditComponent } from './cannon-edit/cannon-edit.component';
import { ShipDetailsComponent } from './ship-details/ship-details.component';
import { GameComponent } from './game/game.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'teams', component: TeamsComponent },
  { path: 'ship', component: ShipComponent },
  { path: 'hull/list', component: HullListComponent },
  { path: 'hull/create', component: HullEditComponent },
  { path: 'hull/update/:id', component: HullEditComponent },
  { path: 'helm/list', component: HelmListComponent },
  { path: 'helm/create', component: HelmEditComponent },
  { path: 'helm/update/:id', component: HelmEditComponent },
  { path: 'sails/list', component: SailsListComponent },
  { path: 'sails/create', component: SailsEditComponent },
  { path: 'sails/update/:id', component: SailsEditComponent },
  { path: 'weapon/list', component: CannonListComponent },
  { path: 'weapon/create', component: CannonEditComponent },
  { path: 'weapon/update/:id', component: CannonEditComponent },
  { path: 'ship/list', component: ShipListComponent },
  { path: 'ship/create', component: ShipEditComponent },
  { path: 'ship/details/:id', component: ShipDetailsComponent },
  { path: 'ship/update/:id', component: ShipEditComponent },
  { path: 'game', component: GameComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
