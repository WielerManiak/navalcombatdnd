import { Component, OnInit } from '@angular/core';
import { Sails } from '../_models/sails';
import { SailsService } from '../_services/sails.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-sails-list',
  templateUrl: './sails-list.component.html',
  styleUrls: ['./sails-list.component.scss'],
})
export class SailsListComponent implements OnInit {
  sails: Sails[];
  loading = false;

  constructor(private sailsService: SailsService, private router: Router) {}

  ngOnInit(): void {
    this.getSails();
  }

  getSails(): void {
    this.sailsService.getSailss().subscribe((sails) => (this.sails = sails));
  }

  onDelete(id: number): void {
    this.loading = true;
    this.sailsService.deleteSails(id).subscribe(() => {
      let index = this.sails.findIndex((h) => h.id === id);
      this.sails.splice(index, 1);
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/sails/list']);
      this.loading = false;
    });
  }
}
