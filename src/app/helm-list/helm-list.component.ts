import { Component, OnInit } from '@angular/core';
import { Helm } from '../_models/helm';
import { HelmService } from '../_services/helm.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-helm-list',
  templateUrl: './helm-list.component.html',
  styleUrls: ['./helm-list.component.scss'],
})
export class HelmListComponent implements OnInit {
  helms: Helm[];
  loading = false;

  constructor(private helmService: HelmService, private router: Router) {}

  ngOnInit(): void {
    this.getHelms();
  }

  getHelms(): void {
    this.helmService.getHelms().subscribe((helms) => (this.helms = helms));
  }

  onDelete(id: number): void {
    this.loading = true;
    this.helmService.deleteHelm(id).subscribe(() => {
      let index = this.helms.findIndex((h) => h.id === id);
      this.helms.splice(index, 1);
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/helm/list']);
      this.loading = false;
    });
  }
}
