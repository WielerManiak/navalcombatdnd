import { Color } from './color';
import { Ship } from './ship';

export class Team {
  name: string;
  ship: Ship;
  color: string;
  hasInitiative: boolean;

  constructor(
    _name: string,
    _ship: Ship,
    _color: string
    ) {
    this.name = _name;
    this.ship = _ship;
    this.color = _color;
  }
}
