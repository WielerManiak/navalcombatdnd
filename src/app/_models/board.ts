import { Ship } from './ship'
import { Weather } from './weather'
import { Team } from './team'
import { Direction } from './direction';

export class Board {
  size: number;
  tiles: object[];
  teams: Team[];
  weather: Weather;
  wind: Direction;

  constructor(_size: number, _teams: Team[], _weather: Weather, _wind: Direction) {
    this.size = _size;
    this.tiles = [];
    this.teams = _teams;
    this.weather = _weather;
    this.wind = _wind;
    this.createBoard()
  }

  createBoard(){
    let _tiles = [] as any;;
    for (let i = 0; i < this.size; i++) {
      _tiles[i] = [];
      for (let j = 0; j < this.size; j++) {
        _tiles[i][j] = { value: 0 };
      }
    }
    this.tiles = _tiles;
    this.randomShip1(this.size)
    this.randomShip2(this.size)
  }

  randomShip1(size: number) {
    size = size -1;
    let row = this.getRandomInt(0, size/3),
    col = this.getRandomInt(size/3, size/1.5);

    this.teams[0].ship.positionX = Math.floor(row);
    this.teams[0].ship.positionY = Math.floor(col);
  }

  randomShip2(size: number) {
    size = size -1;
    let row = this.getRandomInt(size/1.5, size),
    col = this.getRandomInt(size/3, size/1.5);

    this.teams[1].ship.positionX = Math.floor(row);
    this.teams[1].ship.positionY = Math.floor(col);
  }

  getRandomInt(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}
