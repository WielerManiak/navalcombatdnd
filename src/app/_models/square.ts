export class Square {
  private color = '';
  private x = 0;
  private y = 0;
  private z = 10;
  private thickness = 1;

  constructor(private ctx: CanvasRenderingContext2D) {}
  // constructor(private ctx: CanvasRenderingContext2D, _x: number, _y: number){
  //   this.x = _x;
  //   this.y = _y;
  // }

  clear() {
    this.ctx.clearRect(this.z * this.x, this.z * this.y, this.z, this.z);
  }

  clearAll(width: number, height: number){
    this.ctx.clearRect(0, 0, width, height)
  }

  move(x: number, y: number) {
    this.x = x;
    this.y = y;
    this.draw();
  }

  initialize(x: number, y: number, z: number, color: string) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.color = color;

    this.ctx.fillStyle = this.color;
    this.ctx.fillRect(z * x, z * y, z, z);

  }

  draw() {
    
    this.ctx.fillStyle = this.color;
    this.ctx.fillRect(this.z * this.x, this.z * this.y, this.z, this.z);

  }

  drawBorder(){
    this.ctx.fillStyle='#000';
    this.ctx.fillRect((this.z * this.x) - this.thickness, (this.z * this.y) - this.thickness, this.z + (this.thickness ), this.z + (this.thickness ))
  }
}
