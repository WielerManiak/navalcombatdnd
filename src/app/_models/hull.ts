export class Hull {
  public id: number;
  public name: string;
  public AC: number;
  public hitpoints: number;
  public actualHitpoints: number;
  public damageTreshold: number;

  constructor(
    _id: number,
    _name: string,
    _ac: number,
    _hitpoints: number,
    _damageTreshold: number
  ) {
    this.id = _id;
    this.name = _name;
    this.AC = _ac;
    this.hitpoints = _hitpoints;
    this.actualHitpoints = _hitpoints;
    this.damageTreshold = _damageTreshold;
  }

  takeDamage(damage: number): void {
    if (damage >= this.damageTreshold) {
      this.actualHitpoints = this.actualHitpoints - damage;
    } else {
      return;
    }
  }

  repairDamage(_hitpoints: number): void {
    this.actualHitpoints += +_hitpoints;
    if (this.actualHitpoints > this.hitpoints) {
      this.actualHitpoints = this.hitpoints;
    }
  }
}
