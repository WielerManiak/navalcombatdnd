export class Helm {
  public id: number;
  public name: string;
  public AC: number;
  public hitpoints: number;
  public actualHitpoints: number;
  public isDestroyed: boolean;

  constructor(_id: number, _name: string, _ac: number, _hitpoints: number) {
    this.id = _id;
    this.name = _name;
    this.AC = _ac;
    this.hitpoints = _hitpoints;
    this.actualHitpoints = this.hitpoints;
    this.isDestroyed = false;
  }

  takeDamage(damage: number): void {
    this.actualHitpoints = this.actualHitpoints - damage;
    if (this.actualHitpoints <= 0) {
      this.isDestroyed = true;
      this.actualHitpoints = 0;
    }
  }

  repairDamage(_hitpoints: number): void {
    this.actualHitpoints += +_hitpoints;
    if (this.actualHitpoints > this.hitpoints) {
      this.actualHitpoints = this.hitpoints;
    }
  }
}
