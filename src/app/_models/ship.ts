import { Cannon } from './cannon';
import { Direction } from './direction';
import { Helm } from './helm';
import { Hull } from './hull';
import { Sails } from './sails';

export class Ship {
  public id: number;
  public name: string;
  public crew: number;
  public isDecapitated: boolean;
  public size: number;
  public propulsion: string;
  public sails: Sails;
  public hull: Hull;
  public helm: Helm;
  public cannons: Cannon[];
  public facing: Direction;
  public positionX: number;
  public positionY: number;
  public windBonusSpeed: number;

  constructor(
    _id: number,
    _name: string,
    _sails: Sails,
    _crew: number,
    _hull: Hull,
    _helm: Helm,
    _size: number,
    _propulsion: string,
    _cannons: Cannon[],
    _facing?: Direction
  ) {
    this.id = _id;
    this.name = _name;
    this.sails = _sails;
    this.crew = _crew;
    this.hull = _hull;
    this.helm = _helm;
    this.isDecapitated = false;
    this.size = _size;
    this.propulsion = _propulsion;
    this.cannons = _cannons;
    this.facing = _facing || undefined;
    this.positionX = 0;
    this.positionY = 0;
    this.windBonusSpeed = 0;
  }

  hitHull(damage: number) {
    this.hull.takeDamage(damage);
    if (this.hull.actualHitpoints <= 0) {
      this.isDecapitated = true;
      this.hull.actualHitpoints = 0;
      this.sails.actualHitpoints = 0;
      this.helm.actualHitpoints = 0;
      this.helm.isDestroyed = true;
    }
  }

  repairHull(hitpoints: number): void {
    this.hull.repairDamage(hitpoints);
  }

  hitHelm(damage: number) {
    this.helm.takeDamage(damage);
  }

  repairHelm(hitpoints: number) {
    this.helm.repairDamage(hitpoints);
  }

  hitSails(damage: number) {
    this.sails.takeDamage(damage);
  }

  repairSails(hitpoints: number) {
    this.sails.repairDamage(hitpoints);
  }

  hitCannon(damage: number, cannonId: number) {
    this.getCannon(cannonId).takeDamage(damage);
  }

  getCannon(id: number): Cannon {
    return <Cannon>this.cannons.find((x) => x.id === id);
  }

  addCannon(cannon: Cannon): void {
    this.cannons.push(cannon);
  }

  moveNorth(windDirection: Direction) {
    if (!this.isDecapitated) {
      this.facing = Direction.North;
      this.CalculateWindBonusSpeed(windDirection);
      let tilespeed = this.sails.actualSpeed / 30;
      this.positionY = this.positionY - tilespeed;
    }
  }

  moveNorthWest(windDirection: Direction) {
    if (!this.isDecapitated) {
      this.facing = Direction.NorthWest;
      this.CalculateWindBonusSpeed(windDirection);
      let tilespeed = this.sails.actualSpeed / 30 / 1.4;
      this.positionY = this.positionY - tilespeed;
      this.positionX = this.positionX - tilespeed;
    }
  }

  moveWest(windDirection: Direction) {
    if (!this.isDecapitated) {
      this.facing = Direction.West;
      this.CalculateWindBonusSpeed(windDirection);
      let tilespeed = this.sails.actualSpeed / 30;
      this.positionX = this.positionX - tilespeed;
    }
  }

  moveSouthWest(windDirection: Direction) {
    if (!this.isDecapitated) {
      this.facing = Direction.SouthWest;
      this.CalculateWindBonusSpeed(windDirection);
      let tilespeed = this.sails.actualSpeed / 30 / 1.4;
      this.positionY = this.positionY + tilespeed;
      this.positionX = this.positionX - tilespeed;
    }
  }

  moveSouth(windDirection: Direction) {
    if (!this.isDecapitated) {
      this.facing = Direction.South;
      this.CalculateWindBonusSpeed(windDirection);
      let tilespeed = this.sails.actualSpeed / 30;
      this.positionY = this.positionY + tilespeed;
    }
  }

  moveSouthEast(windDirection: Direction) {
    if (!this.isDecapitated) {
      this.facing = Direction.SouthEast;
      this.CalculateWindBonusSpeed(windDirection);
      let tilespeed = this.sails.actualSpeed / 30 / 1.4;
      this.positionY = this.positionY + tilespeed;
      this.positionX = this.positionX + tilespeed;
    }
  }

  moveEast(windDirection: Direction) {
    if (!this.isDecapitated) {
      this.facing = Direction.East;
      this.CalculateWindBonusSpeed(windDirection);
      let tilespeed = this.sails.actualSpeed / 30;
      this.positionX = this.positionX + tilespeed;
    }
  }

  moveNorthEast(windDirection: Direction) {
    if (!this.isDecapitated) {
      this.facing = Direction.NorthEast;
      this.CalculateWindBonusSpeed(windDirection);
      let tilespeed = this.sails.actualSpeed / 30 / 1.4;
      this.positionY = this.positionY - tilespeed;
      this.positionX = this.positionX + tilespeed;
    }
  }

  CalculateWindBonusSpeed(windDirection: Direction) {
    if (this.facing == windDirection) {
      this.windBonusSpeed = 15;
    } else {
      this.windBonusSpeed = 0;
    }

    switch (this.facing) {
      case Direction.North:
        if (windDirection == Direction.South) {
          this.windBonusSpeed = -10;
        }
        break;
      case Direction.NorthEast:
        if (windDirection == Direction.SouthWest) {
          this.windBonusSpeed = -10;
        }
        break;
      case Direction.East:
        if (windDirection == Direction.West) {
          this.windBonusSpeed = -10;
        }
        break;
      case Direction.SouthEast:
        if (windDirection == Direction.NorthWest) {
          this.windBonusSpeed = -10;
        }
        break;
      case Direction.South:
        if (windDirection == Direction.North) {
          this.windBonusSpeed = -10;
        }
        break;
      case Direction.SouthWest:
        if (windDirection == Direction.NorthEast) {
          this.windBonusSpeed = -10;
        }
        break;
      case Direction.West:
        if (windDirection == Direction.East) {
          this.windBonusSpeed = -10;
        }
        break;
      case Direction.NorthWest:
        if (windDirection == Direction.SouthEast) {
          this.windBonusSpeed = -10;
        }
        break;
      default:
        this.windBonusSpeed = 0;
        break;
    }
    this.sails.calculateSpeed();
    this.sails.actualSpeed = this.sails.actualSpeed + this.windBonusSpeed;
  }
}
