export enum Weather {
  Clear = "Clear",
  Overcast = "Overcast",
  Raining = "Raining",
  Fog = "Fog"
}
