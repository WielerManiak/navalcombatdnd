import { Position } from './position';

export class Cannon {
  public id: number;
  public name: string;
  public hitDice: string;
  public damageDice: string;
  public AC: number;
  public hitpoints: number;
  public range: number;
  public disadvantageRange: number;
  public isLoaded: boolean;
  public isBroken: boolean;
  public crew: number;
  public position: Position;

  constructor(
    _id: number,
    _name: string,
    _hitDice: string,
    _damageDice: string,
    _ac: number,
    _hitpoints: number,
    _range: number,
    _disadvantageRange: number,
    _crew: number,
    _position?: Position
  ) {
      (this.id = _id),
      (this.name = _name),
      (this.hitDice = _hitDice),
      (this.damageDice = _damageDice),
      (this.AC = _ac);
      (this.hitpoints = _hitpoints),
      (this.range = _range),
      (this.disadvantageRange = _disadvantageRange),
      (this.isLoaded = false),
      (this.isBroken = false),
      (this.crew = _crew),
      (this.position = _position || undefined);
  }

  takeDamage(damage: number) {
    this.hitpoints = this.hitpoints - damage;
    if (this.hitpoints <= 0) {
      this.isBroken = true;
    }
  }
}
