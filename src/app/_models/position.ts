export enum Position {
  Bow = "Bow",
  Port = "Port",
  Starboard = "Starboard",
  Stern = "Stern"
}
