export enum Direction {
  North = "North",
  NorthWest = "NorthWest",
  West = "West",
  SouthWest = "SouthWest",
  South = "South",
  SouthEast = "SouthEast",
  East = "East",
  NorthEast = "NorthEast"
}
