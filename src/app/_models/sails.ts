export class Sails {
  public id: number;
  public name: string;
  public AC: number;
  public hitpoints: number;
  public actualHitpoints: number;
  public speed: number;
  public actualSpeed: number;

  constructor(
    _id: number,
    _name: string,
    _ac: number,
    _hitpoint: number,
    _speed: number
  ) {
    this.id = _id;
    this.name = _name;
    this.AC = _ac;
    this.hitpoints = _hitpoint;
    this.actualHitpoints = _hitpoint;
    this.speed = _speed;
    this.actualSpeed = _speed;
  }

  takeDamage(damage: number) {
    this.actualHitpoints = this.actualHitpoints - damage;
    this.calculateSpeed();
  }

  repairDamage(_hitpoints: number) {
    this.actualHitpoints += +_hitpoints;
    if (this.actualHitpoints > this.hitpoints) {
      this.actualHitpoints = this.hitpoints;
    }
    this.calculateSpeed();
  }

  calculateSpeed(): void {
    if (this.actualHitpoints <= this.hitpoints) {
      this.actualSpeed = this.speed;
    }
    if (this.actualHitpoints <= this.hitpoints * 0.75) {
      this.actualSpeed = this.speed * 0.75;
    }
    if (this.actualHitpoints <= this.hitpoints * 0.5) {
      this.actualSpeed = this.speed * 0.5;
    }
    if (this.actualHitpoints <= this.hitpoints * 0.25) {
      this.actualSpeed = this.speed * 0.25;
    }
    if (this.actualHitpoints <= 0) {
      this.actualSpeed = 0;
      this.actualHitpoints = 0;
    }
  }
}
