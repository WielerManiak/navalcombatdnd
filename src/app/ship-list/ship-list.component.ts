import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ship } from '../_models/ship';
import { ShipService } from '../_services/ship.service';

@Component({
  selector: 'app-ship-list',
  templateUrl: './ship-list.component.html',
  styleUrls: ['./ship-list.component.scss'],
})
export class ShipListComponent implements OnInit {
  ships: Ship[];
  loading = false;

  constructor(private shipService: ShipService, private router: Router) {}

  ngOnInit(): void {
    this.getShips();
  }

  getShips(): void {
    this.shipService.getShips().subscribe((ships) => (this.ships = ships));
  }

  onDelete(id: number): void {
    this.loading = true;
    this.shipService.deleteShip(id).subscribe(() => {
      let index = this.ships.findIndex((h) => h.id === id);
      this.ships.splice(index, 1);
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/ship/list']);
      this.loading = false;
    });
  }
}
