import { Component, OnInit } from '@angular/core';
import { Cannon } from '../_models/cannon';
import { ActivatedRoute, Router } from '@angular/router';
import { CannonService } from '../_services/cannon.service';

@Component({
  selector: 'app-cannon-list',
  templateUrl: './cannon-list.component.html',
  styleUrls: ['./cannon-list.component.scss']
})
export class CannonListComponent implements OnInit {
  cannons: Cannon[];
  loading = false;

  constructor(private cannonService: CannonService, private router: Router) { }

  ngOnInit(): void {
    this.getCannons();
  }

  getCannons(): void {
    this.cannonService.getCannons().subscribe((cannons) => (this.cannons = cannons));
  }

  onDelete(id: number): void {
    this.loading = true;
    this.cannonService.deleteCannon(id).subscribe(() => {
      let index = this.cannons.findIndex((h) => h.id === id);
      this.cannons.splice(index, 1);
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/cannon/list']);
      this.loading = false;
    });
  }
}
