import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShipComponent } from './ship/ship.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataServiceService } from '../app/_services/in-memory-data-service.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TeamsComponent } from './teams/teams.component';
import { HeaderComponent } from './header/header.component';
import { ShipListComponent } from './ship-list/ship-list.component';
import { ShipEditComponent } from './ship-edit/ship-edit.component';
import { HullListComponent } from './hull-list/hull-list.component';
import { HullEditComponent } from './hull-edit/hull-edit.component';
import { HelmListComponent } from './helm-list/helm-list.component';
import { HelmEditComponent } from './helm-edit/helm-edit.component';
import { SailsListComponent } from './sails-list/sails-list.component';
import { SailsEditComponent } from './sails-edit/sails-edit.component';
import { CannonListComponent } from './cannon-list/cannon-list.component';
import { CannonEditComponent } from './cannon-edit/cannon-edit.component';
import { ShipDetailsComponent } from './ship-details/ship-details.component';
import { Data } from './_helpers/data';
import { GameComponent } from './game/game.component';

@NgModule({
  declarations: [
    AppComponent,
    ShipComponent,
    DashboardComponent,
    TeamsComponent,
    HeaderComponent,
    ShipListComponent,
    ShipEditComponent,
    HullListComponent,
    HullEditComponent,
    HelmListComponent,
    HelmEditComponent,
    SailsListComponent,
    SailsEditComponent,
    CannonListComponent,
    CannonEditComponent,
    ShipDetailsComponent,
    GameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,

    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataServiceService, {
      dataEncapsulation: false
    })
  ],
  providers: [Data],
  bootstrap: [AppComponent]
})
export class AppModule {}
