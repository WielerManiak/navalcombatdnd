import {
  Component,
  ViewChild,
  ElementRef,
  OnInit,
} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Data } from '../_helpers/data';
import { Board } from '../_models/board';
import { Ship } from '../_models/ship';
import { Direction } from '../_models/direction';
import { Weather } from '../_models/weather';
import { Helm } from '../_models/helm';
import { Hull } from '../_models/hull';
import { Sails } from '../_models/sails';
import { Square } from '../_models/square';
import { enumSelector } from '../_helpers/enumSelector';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit {
  @ViewChild('canvas', { static: false })
  canvas: ElementRef<HTMLCanvasElement>;
  ctx: CanvasRenderingContext2D;
  squares: Square[] = [];
  width = 695;
  height = 610;

  board: Board;
  ship1: Ship;
  ship2: Ship;

  square1: Square;
  square2: Square;

  BoatDirection = enumSelector(Direction);
  weather = enumSelector(Weather);
  distanceBetweenShips = 0;

  constructor(
    private data: Data,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.board = <Board>this.data.storage;
    this.ship1 = new Ship(
      undefined,
      this.board.teams[0].ship.name,
      new Sails(
        undefined,
        this.board.teams[0].ship.sails.name,
        this.board.teams[0].ship.sails.AC,
        this.board.teams[0].ship.sails.hitpoints,
        this.board.teams[0].ship.sails.speed
      ),
      this.board.teams[0].ship.crew,
      new Hull(
        undefined,
        this.board.teams[0].ship.hull.name,
        this.board.teams[0].ship.hull.AC,
        this.board.teams[0].ship.hull.hitpoints,
        this.board.teams[0].ship.hull.damageTreshold
      ),
      new Helm(
        undefined,
        this.board.teams[0].ship.helm.name,
        this.board.teams[0].ship.helm.AC,
        this.board.teams[0].ship.helm.hitpoints
      ),
      this.board.teams[0].ship.size,
      this.board.teams[0].ship.propulsion,
      this.board.teams[0].ship.cannons,
      this.board.teams[0].ship.facing
    );
    this.ship2 = new Ship(
      undefined,
      this.board.teams[1].ship.name,
      new Sails(
        undefined,
        this.board.teams[1].ship.sails.name,
        this.board.teams[1].ship.sails.AC,
        this.board.teams[1].ship.sails.hitpoints,
        this.board.teams[1].ship.sails.speed
      ),
      this.board.teams[1].ship.crew,
      new Hull(
        undefined,
        this.board.teams[1].ship.hull.name,
        this.board.teams[1].ship.hull.AC,
        this.board.teams[1].ship.hull.hitpoints,
        this.board.teams[1].ship.hull.damageTreshold
      ),
      new Helm(
        undefined,
        this.board.teams[1].ship.helm.name,
        this.board.teams[1].ship.helm.AC,
        this.board.teams[1].ship.helm.hitpoints
      ),
      this.board.teams[1].ship.size,
      this.board.teams[1].ship.propulsion,
      this.board.teams[1].ship.cannons,
      this.board.teams[1].ship.facing
    );
    console.log(this.board);
    this.ship1.positionX = this.board.teams[0].ship.positionX;
    this.ship1.positionY = this.board.teams[0].ship.positionY;

    this.ship2.positionX = this.board.teams[1].ship.positionX;
    this.ship2.positionY = this.board.teams[1].ship.positionY;

    this.getDistance();
  }

  ngAfterViewInit() {
    this.ctx = this.canvas.nativeElement.getContext('2d')
    this.animate();
  }

  animate() {
    this.drawGrid();
    this.square1 = new Square(this.ctx);
    this.square2 = new Square(this.ctx);
    this.square1.initialize(
      this.board.teams[0].ship.positionX,
      this.board.teams[0].ship.positionY,
      10,
      this.board.teams[0].color
    );

    this.square2.initialize(
      this.board.teams[1].ship.positionX,
      this.board.teams[1].ship.positionY,
      10,
      this.board.teams[1].color
    );
  }

  drawGrid() {
    let s = 28;
    let nX = Math.floor(this.width / s) - 2;
    let nY = Math.floor(this.height / s) - 2;
    let pX = this.width - nX * s;
    let pY = this.height - nY * s;
    let pL = Math.ceil(pX / 2) - 0.5;
    let pT = Math.ceil(pY / 2) - 0.5;
    let pR = this.width - nX * s - pL;
    let pB = this.height - nY * s - pT;

    this.ctx.strokeStyle = 'lightgrey';
    this.ctx.beginPath();
    for (var x = 0; x <= this.width; x += s) {
      this.ctx.moveTo(x, 0);
      this.ctx.lineTo(x, this.height);
    }
    for (var y = 0; y <= this.height; y += s) {
      this.ctx.moveTo(0, y);
      this.ctx.lineTo(this.width, y);
    }
    this.ctx.stroke();
  }

  hitHullTest() {
    this.ship1.hitHull(50);
  }

  getDistance() {
    let x = Math.abs(this.ship1.positionX - this.ship2.positionX);
    let y = Math.abs(this.ship1.positionY - this.ship2.positionY);

    let distance = Math.sqrt(x * x + y * y);
    this.distanceBetweenShips = Math.floor(distance * 30);
  }

  hitHullShip1(damage: number) {
    this.ship1.hitHull(damage);
  }

  repairHullShipOne(hitpoints: number) {
    this.ship1.repairHull(hitpoints);
  }

  hitHelmShip1(damage: number) {
    this.ship1.hitHelm(damage);
  }

  repairHelmShipOne(hitpoints: number) {
    this.ship1.repairHelm(hitpoints);
  }

  hitSailsShip1(damage: number) {
    this.ship1.hitSails(damage);
  }

  repairSailsShipOne(hitpoints: number) {
    this.ship1.repairSails(hitpoints);
  }

  /////////////////////////////////////

  hitHullShip2(damage: number) {
    this.ship2.hitHull(damage);
  }

  repairHullShipTwo(hitpoints: number) {
    this.ship2.repairHull(hitpoints);
  }

  hitHelmShip2(damage: number) {
    this.ship2.hitHelm(damage);
  }

  repairHelmShipTwo(hitpoints: number) {
    this.ship2.repairHelm(hitpoints);
  }

  hitSailsShip2(damage: number) {
    this.ship2.hitSails(damage);
  }

  repairSailsShipTwo(hitpoints: number) {
    this.ship2.repairSails(hitpoints);
  }

  /////////////////////////////////////

  openShipModal(content) {
    this.modalService.open(content);
  }

  onEditWindClick(direction: any) {
    this.board.wind = direction;
  }

  onEditWeatherClick(weather: any) {
    this.board.weather = weather;
  }

  onEditShip1FacingClick(direction: any) {
    this.ship1.facing = direction;
  }

  onEditShip2FacingClick(direction: any) {
    this.ship2.facing = direction;
  }

  moveShip1North() {
    this.ship1.moveNorth(this.board.wind);
    this.moveSquare1();
    this.getDistance();
  }

  moveShip2North() {
    this.ship2.moveNorth(this.board.wind);
    this.moveSquare2();
    this.getDistance();
  }

  moveShip1NorthEast() {
    this.ship1.moveNorthEast(this.board.wind);
    this.moveSquare1();
    this.getDistance();
  }

  moveShip2NorthEast() {
    this.ship2.moveNorthEast(this.board.wind);
    this.moveSquare2();
    this.getDistance();
  }

  moveShip1East() {
    this.ship1.moveEast(this.board.wind);
    this.moveSquare1();
    this.getDistance();
  }

  moveShip2East() {
    this.ship2.moveEast(this.board.wind);
    this.moveSquare2();
    this.getDistance();
  }

  moveShip1SouthEast() {
    this.ship1.moveSouthEast(this.board.wind);
    this.moveSquare1();
    this.getDistance();
  }

  moveShip2SouthEast() {
    this.ship2.moveSouthEast(this.board.wind);
    this.moveSquare2();
    this.getDistance();
  }

  moveShip1South() {
    this.ship1.moveSouth(this.board.wind);
    this.moveSquare1();
    this.getDistance();
  }

  moveShip2South() {
    this.ship2.moveSouth(this.board.wind);
    this.moveSquare2();
    this.getDistance();
  }

  moveShip1SouthWest() {
    this.ship1.moveSouthWest(this.board.wind);
    this.moveSquare1();
    this.getDistance();
  }

  moveShip2SouthWest() {
    this.ship2.moveSouthWest(this.board.wind);
    this.moveSquare2();
    this.getDistance();
  }

  moveShip1West() {
    this.ship1.moveWest(this.board.wind);
    this.moveSquare1();
    this.getDistance();
  }

  moveShip2West() {
    this.ship2.moveWest(this.board.wind);
    this.moveSquare2();
    this.getDistance();
  }

  moveShip1NorthWest() {
    this.ship1.moveNorthWest(this.board.wind);
    this.moveSquare1();
    this.getDistance();
  }

  moveShip2NorthWest() {
    this.ship2.moveNorthWest(this.board.wind);
    this.moveSquare2();
    this.getDistance();
  }

  moveSquare1() {
    this.square1.clearAll(this.width, this.height);
    this.square1.move(this.ship1.positionX, this.ship1.positionY);
    this.square1.draw();

    this.square2.draw();
    this.drawGrid();
  }

  moveSquare2() {
    this.square1.clearAll(this.width, this.height);
    this.square2.move(this.ship2.positionX, this.ship2.positionY);
    this.square2.draw();

    this.square1.draw();
    this.drawGrid();
  }
}
