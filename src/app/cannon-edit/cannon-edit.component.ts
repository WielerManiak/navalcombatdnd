import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Cannon } from '../_models/cannon';
import { CannonService } from '../_services/cannon.service';
import { updateDecorator } from 'typescript';

@Component({
  selector: 'app-cannon-edit',
  templateUrl: './cannon-edit.component.html',
  styleUrls: ['./cannon-edit.component.scss'],
})
export class CannonEditComponent implements OnInit {
  cannonForm: FormGroup;
  cannon: Cannon;
  id: number;
  isCreateMode: boolean;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private cannonService: CannonService
  ) {}

  ngOnInit(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.isCreateMode = !this.id;

    if (this.isCreateMode) {
      this.cannon = new Cannon(
        undefined,
        '',
        '',
        '',
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined
      );

      this.cannonForm = new FormGroup({
        name: new FormControl('', [Validators.required]),
        AC: new FormControl('', [Validators.required]),
        hitpoints: new FormControl('', [Validators.required]),
        hitDice: new FormControl('', [Validators.required]),
        damageDice: new FormControl('', [Validators.required]),
        range: new FormControl('', [Validators.required]),
        disadvantageRange: new FormControl('', [Validators.required]),
        crew: new FormControl('', [Validators.required]),
      });
    }

    if (!this.isCreateMode) {
      this.getCannon(this.id);
    }
  }

  get f() {
    return this.cannonForm.controls;
  }

  getCannon(id: number) {
    this.cannonService.getCannon(id).subscribe((cannon) => {
      this.cannon = cannon;

      this.cannonForm = new FormGroup({
        name: new FormControl(this.cannon.name, [Validators.required]),
        AC: new FormControl(this.cannon.AC, [Validators.required]),
        hitpoints: new FormControl(this.cannon.hitpoints, [
          Validators.required,
        ]),
        hitDice: new FormControl(this.cannon.hitDice, [Validators.required]),
        damageDice: new FormControl(this.cannon.damageDice, [
          Validators.required,
        ]),
        range: new FormControl(this.cannon.range, [Validators.required]),
        disadvantageRange: new FormControl(this.cannon.disadvantageRange, [
          Validators.required,
        ]),
        crew: new FormControl(this.cannon.crew, [Validators.required]),
      });
    });
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;

    if (this.cannonForm.invalid) {
      return;
    }

    if (this.isCreateMode) {
      this.createCannon();
    } else {
      this.updateCannon();
    }
  }

  private createCannon() {
    this.cannon = this.cannonForm.value;

    this.cannonService.addCannon(this.cannon).subscribe(() => {
      this.goBack();
      this.loading = false;
    });
  }

  private updateCannon() {
    this.cannon.name = this.cannonForm.value.name;
    this.cannon.AC = this.cannonForm.value.AC;
    this.cannon.hitpoints = this.cannonForm.value.hitpoints;
    this.cannon.hitDice = this.cannonForm.value.hitDice;
    this.cannon.damageDice = this.cannonForm.value.damageDice;
    this.cannon.range = this.cannonForm.value.range;
    this.cannon.disadvantageRange = this.cannonForm.value.disadvantageRange;
    this.cannon.crew = this.cannonForm.value.crew;

    this.cannonService.updateCannon(this.cannon).subscribe(() => {
      this.goBack();
      this.loading = false;
    })
  }

  goBack(): void {
    this.router.navigate(['/cannon/list']);
  }
}
