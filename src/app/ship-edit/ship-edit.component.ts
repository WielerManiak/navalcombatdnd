import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
  FormArray,
} from '@angular/forms';
import { Ship } from '../_models/ship';
import { ShipService } from '../_services/ship.service';
import { Hull } from '../_models/hull';
import { Helm } from '../_models/helm';
import { Sails } from '../_models/sails';
import { Cannon } from '../_models/cannon';
import { HullService } from '../_services/hull.service';
import { HelmService } from '../_services/helm.service';
import { SailsService } from '../_services/sails.service';
import { CannonService } from '../_services/cannon.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Position } from '../_models/position';
import { enumSelector } from '../_helpers/enumSelector';

@Component({
  selector: 'app-ship-edit',
  templateUrl: './ship-edit.component.html',
  styleUrls: ['./ship-edit.component.scss'],
})
export class ShipEditComponent implements OnInit {
  shipForm: FormGroup;
  cannonForm: FormGroup;
  hulls: Hull[];
  helms: Helm[];
  sails: Sails[];
  cannons: Cannon[];
  ship: Ship;
  id: number;
  isCreateMode: boolean;
  loading = false;
  submitted = false;
  selectedBowCannons: Cannon[] = [];
  selectedPortCannons: Cannon[] = [];
  selectedStarboardCannons: Cannon[] = [];
  selectedSternCannons: Cannon[] = [];
  closeResult = '';
  positions = enumSelector(Position);

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private shipService: ShipService,
    private hullService: HullService,
    private helmService: HelmService,
    private sailsService: SailsService,
    private cannonService: CannonService,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.isCreateMode = !this.id;

    if (this.isCreateMode) {
      this.ship = new Ship(
        undefined,
        '',
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        '',
        [],
        undefined
      );

      this.shipForm = new FormGroup({
        name: new FormControl('', [Validators.required]),
        crew: new FormControl('', [Validators.required]),
        hull: new FormControl('', [Validators.required]),
        helm: new FormControl('', [Validators.required]),
        sails: new FormControl('', [Validators.required])
      });

      this.cannonForm = new FormGroup({
        cannon: new FormControl('', [Validators.required]),
        position: new FormControl('', [Validators.required]),
        amount: new FormControl('', [Validators.required]),
      });

      this.hullService.getHulls().subscribe((hulls) => (this.hulls = hulls));
      this.helmService.getHelms().subscribe((helms) => (this.helms = helms));
      this.sailsService.getSailss().subscribe((sails) => (this.sails = sails));
      this.cannonService.getCannons().subscribe((cannons) => {
        this.cannons = cannons;
      });
    }
  }

  get f() {
    return this.shipForm.controls;
  }

  get c() {
    return this.cannonForm.controls;
  }

  open(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'exampleModalLabel' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;

    if(this.shipForm.invalid){
      return;
    }

    this.ship.name = this.shipForm.value.name;
    this.ship.crew = this.shipForm.value.crew;
    this.ship.hull = this.shipForm.value.hull;
    this.ship.helm = this.shipForm.value.helm;
    this.ship.sails = this.shipForm.value.sails;

    this.shipService.addShip(this.ship).subscribe(() =>{
      this.goBack();
      this.loading = false;
    })

  }

  onCannonSubmit() {
    for (let i = 0; i < this.cannonForm.value.amount; i++) {
      let newCannon = new Cannon(
        this.cannonForm.value.cannon.id,
        this.cannonForm.value.cannon.name,
        this.cannonForm.value.cannon.hitDice,
        this.cannonForm.value.cannon.damageDice,
        this.cannonForm.value.cannon.AC,
        this.cannonForm.value.cannon.hitpoints,
        this.cannonForm.value.cannon.range,
        this.cannonForm.value.cannon.disadvantageRange,
        this.cannonForm.value.cannon.crew,
        this.cannonForm.value.position
      );
      this.ship.addCannon(newCannon)
    }
  }

  goBack(): void {
    this.router.navigate(['/ship/list']);
  }
}
