import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Helm } from '../_models/helm';
import { HelmService } from '../_services/helm.service';

@Component({
  selector: 'app-helm-edit',
  templateUrl: './helm-edit.component.html',
  styleUrls: ['./helm-edit.component.scss']
})
export class HelmEditComponent implements OnInit {
  helmForm: FormGroup;
  helm: Helm;
  id: number;
  isCreateMode: boolean;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private helmService: HelmService
  ) { }

  ngOnInit(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.isCreateMode = !this.id;

    if(this.isCreateMode){
      this.helm = new Helm(undefined, '', undefined, undefined);

      this.helmForm = new FormGroup({
        name: new FormControl('', [Validators.required]),
        AC: new FormControl('', [Validators.required]),
        hitpoints: new FormControl('', [Validators.required])
      });
    }

    if(!this.isCreateMode){
      this.getHelm(this.id);
    }
  }

  get f() {
    return this.helmForm.controls;
  }

  getHelm(id: number){
    this.helmService.getHelm(id).subscribe((helm) => {
      this.helm = helm;

      this.helmForm = new FormGroup({
        name: new FormControl(this.helm.name, [Validators.required]),
        AC: new FormControl(this.helm.AC, [Validators.required]),
        hitpoints: new FormControl(this.helm.hitpoints, [Validators.required])
      });
    })
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;

    if(this.helmForm.invalid){
      return;
    }

    if(this.isCreateMode){
      this.createHelm();
    }else{
      this.updateHelm();
    }
  }

  private createHelm(){
    this.helm = this.helmForm.value;

    this.helmService.addHelm(this.helm).subscribe(() => {
      this.goBack();
      this.loading = false;
    });

  }

  private updateHelm(){
    this.helm.name = this.helmForm.value.name;
    this.helm.AC = this.helmForm.value.AC;
    this.helm.hitpoints = this.helmForm.value.hitpoints;

    this.helmService.updateHelm(this.helm).subscribe(() => {
      this.goBack();
      this.loading = false;
    })

  }

  goBack(): void {
    this.router.navigate(['/helm/list']);
  }

}
