import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Sails } from '../_models/sails';
import { SailsService } from '../_services/sails.service';

@Component({
  selector: 'app-sails-edit',
  templateUrl: './sails-edit.component.html',
  styleUrls: ['./sails-edit.component.scss']
})
export class SailsEditComponent implements OnInit {
  sailsForm: FormGroup;
  sails: Sails;
  id: number;
  isCreateMode: boolean;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private sailsService: SailsService
  ) { }

  ngOnInit(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.isCreateMode = !this.id;

    if(this.isCreateMode) {
      this.sails = new Sails(undefined, '', undefined, undefined, undefined)

      this.sailsForm = new FormGroup({
        name: new FormControl('', [Validators.required]),
        AC: new FormControl('', [Validators.required]),
        hitpoints: new FormControl('', [Validators.required]),
        speed: new FormControl('', [Validators.required]),
      })
    }

    if(!this.isCreateMode){
      this.getSails(this.id);
    }
  }

  get f() {
    return this.sailsForm.controls;
  }

  getSails(id: number){
    this.sailsService.getSails(id).subscribe((sails) => {
      this.sails = sails;

      this.sailsForm = new FormGroup({
        name: new FormControl(this.sails.name || '', [Validators.required]),
        AC: new FormControl(this.sails.AC, [Validators.required]),
        hitpoints: new FormControl(this.sails.hitpoints, [Validators.required]),
        speed: new FormControl(this.sails.speed, [Validators.required]),
      });
    })
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;

    if(this.sailsForm.invalid){
      return;
    }

    if(this.isCreateMode) {
      this.createSails();
    } else {
      this.updateSails();
    }
  }

  private createSails(){
    this.sails = this.sailsForm.value;

    this.sailsService.addSails(this.sails).subscribe(() => {
      this.goBack();
      this.loading = false;
    })
  }

  private updateSails(){
    this.sails.name = this.sailsForm.value.name;
    this.sails.AC = this.sailsForm.value.AC;
    this.sails.hitpoints = this.sailsForm.value.hitpoints;
    this.sails.speed = this.sailsForm.value.speed;

    this.sailsService.updateSails(this.sails).subscribe(() => {
      this.goBack();
      this.loading = false;
    })
  }


  goBack(): void {
    this.router.navigate(['/sails/list']);
  }
}
