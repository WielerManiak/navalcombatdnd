import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ship } from '../_models/ship';
import { ShipService } from '../_services/ship.service';
import { Position } from '../_models/position';

@Component({
  selector: 'app-ship-details',
  templateUrl: './ship-details.component.html',
  styleUrls: ['./ship-details.component.scss'],
})
export class ShipDetailsComponent implements OnInit {
  ship: Ship;
  id: number;
  position: Position;

  constructor(
    private shipService: ShipService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.id = +this.route.snapshot.paramMap.get('id');

    this.shipService.getShip(this.id).subscribe((ship) => {
      this.ship = ship;
      console.log(ship);
    })
  }
}
