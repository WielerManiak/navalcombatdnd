import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Cannon } from '../_models/cannon'

@Injectable({
  providedIn: 'root'
})
export class CannonService {

  constructor(private http: HttpClient) { }

  private cannonsUrl = 'api/cannons';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  getCannons(): Observable<Cannon[]> {
    return this.http.get<Cannon[]>(this.cannonsUrl).pipe(
      tap(_ => this.log('fetched cannons')),
      catchError(this.handleError<Cannon[]>('getCannons', []))
    );
  }

  getCannon(id: number): Observable<Cannon> {
    const url = `${this.cannonsUrl}/${id}`;

    return this.http.get<Cannon>(url).pipe(
      tap(_ => this.log(`fetched cannon id=${id}`)),
      catchError(this.handleError<any>(`getCannon id=${id}`))
    )
  }

  addCannon(cannon: Cannon): Observable<any> {
    return this.http.post<Cannon>(this.cannonsUrl, cannon, this.httpOptions).pipe(
      tap(_ => this.log('added cannon')),
      catchError(this.handleError<Cannon>('addCannon'))
    );
  }

  updateCannon(cannon: Cannon): Observable<any> {
    return this.http.put(this.cannonsUrl, cannon, this.httpOptions).pipe(
      tap(_ => this.log(`updated cannon with id=${cannon.id}`)),
      catchError(this.handleError<any>('updateCannon'))
    );
  }

  deleteCannon(cannon: Cannon | number): Observable<any>{
    const id = typeof cannon === 'number' ? cannon : cannon.id;
    const url = `${this.cannonsUrl}/${id}`;

    return this.http.delete<Cannon>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted cannon id=${id}`)),
      catchError(this.handleError<Cannon>('deletedCannon'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(`CannonService: ${message}`);
  }
}
