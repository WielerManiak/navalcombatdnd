import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CannonService } from './cannon.service';

describe('CannonService', () => {
  let service: CannonService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])]
    });
    service = TestBed.inject(CannonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
