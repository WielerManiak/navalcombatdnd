import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Cannon } from '../_models/cannon';
import { Position } from '../_models/position';
import { Ship } from '../_models/ship';
import { Helm } from '../_models/helm';
import { Hull } from '../_models/hull';
import { Sails } from '../_models/sails';
import { Direction } from '../_models/direction';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataServiceService implements InMemoryDbService {
  createDb() {


    const cannonMock: Cannon[] = [
      new Cannon(
        1,
        '18-Pound Cannon',
        '1d20+6',
        '8d10',
        17,
        75,
        600,
        2400,
        3,
        Position.Port
      ),
      new Cannon(
        1,
        '18-Pound Cannon',
        '1d20+6',
        '8d10',
        17,
        75,
        600,
        2400,
        3,
        Position.Starboard
      ),
      new Cannon(
        2,
        '12-Pound Cannon',
        '1d20+6',
        '6d10',
        17,
        60,
        600,
        2400,
        3,
        Position.Port
      ),
      new Cannon(
        2,
        '12-Pound Cannon',
        '1d20+6',
        '6d10',
        17,
        60,
        600,
        2400,
        3,
        Position.Starboard
      ),
      new Cannon(
        3,
        '9-Pound Cannon',
        '1d20+6',
        '4d10',
        18,
        45,
        500,
        2000,
        2,
        Position.Port
      ),
      new Cannon(
        3,
        '9-Pound Cannon',
        '1d20+6',
        '4d10',
        18,
        45,
        500,
        2000,
        2,
        Position.Starboard
      ),
    ]

    const cannons: Cannon[] = [
      new Cannon(
        1,
        '18-Pound Cannon',
        '1d20+6',
        '8d10',
        17,
        75,
        600,
        2400,
        3,
        undefined
      ),
      new Cannon(
        2,
        '12-Pound Cannon',
        '1d20+6',
        '6d10',
        17,
        60,
        600,
        2400,
        3,
        undefined
      ),
      new Cannon(
        3,
        '9-Pound Cannon',
        '1d20+6',
        '4d10',
        18,
        45,
        500,
        2000,
        2,
        undefined
      ),
      new Cannon(
        4,
        'Balistas',
        '1d20+6',
        '3d10',
        15,
        100,
        120,
        480,
        2,
        undefined
      ),
      new Cannon(
        5,
        'Mangonels',
        '1d20+6',
        '5d10',
        15,
        100,
        200,
        800,
        2,
        undefined
      ),
      new Cannon(
        6,
        'Battering Ram',
        '1d20+6',
        'based on speed',
        20,
        100,
        0,
        0,
        0,
        Position.Bow
      ),
    ];

    const helms: Helm[] = [
      new Helm(1, 'Normal Helm', 16, 50),
      new Helm(2, 'Armored Helm', 17, 65)
    ];
    const hulls: Hull[] = [
      new Hull(1, 'Galileon Hull', 15, 1000, 20),
      new Hull(2, 'Brigantine Hull', 15, 500, 20),
      new Hull(3, 'Longship Hull', 15, 300, 15),
      new Hull(4, 'Sloop Hull', 15, 250, 15),
    ];
    const sails: Sails[] = [
      new Sails(1, '3 mast sails ~Galileon', 12, 200, 45),
      new Sails(2, '2 mast sails ~Brigantine', 12, 150, 55),
      new Sails(3, '1 mast sails ~Sloop', 12, 100, 55),
      new Sails(3, '1 mast sails ~Longship', 12, 100, 45),
    ];

    const ships: Ship[] = [
      new Ship(
        1,
        'Galleon',
        sails[0],
        20,
        hulls[0],
        helms[0],
        3,
        'wind',
        [
          cannonMock[0],
          cannonMock[0],
          cannonMock[0],
          cannonMock[0],
          cannonMock[1],
          cannonMock[1],
          cannonMock[1],
          cannonMock[1],
          cannonMock[2],
          cannonMock[2],
          cannonMock[2],
          cannonMock[2],
          cannonMock[2],
          cannonMock[2],
          cannonMock[3],
          cannonMock[3],
          cannonMock[3],
          cannonMock[3],
          cannonMock[3],
          cannonMock[3],
        ],
        Direction.East
      ),
      new Ship(
        2,
        'Brigantine',
        sails[1],
        20,
        hulls[1],
        helms[0],
        3,
        'wind',
        [
          cannonMock[2],
          cannonMock[2],
          cannonMock[2],
          cannonMock[2],
          cannonMock[2],
          cannonMock[2],
          cannonMock[3],
          cannonMock[3],
          cannonMock[3],
          cannonMock[3],
          cannonMock[3],
          cannonMock[3],
        ],
        Direction.East
      ),
      new Ship(
        3,
        'Longship',
        sails[3],
        20,
        hulls[2],
        helms[0],
        3,
        'wind',
        [],
        Direction.East
      ),
      new Ship(
        4,
        'Sloop',
        sails[2],
        20,
        hulls[3],
        helms[0],
        3,
        'wind',
        [
          cannonMock[4],
          cannonMock[4],
          cannonMock[4],
          cannonMock[5],
          cannonMock[5],
          cannonMock[5],
        ],
        Direction.East
      ),
    ];
    return { cannons, helms, hulls, sails, ships };
  }

  genId<T extends Hull | Helm | Sails | Cannon | Ship>(myTable: T[]): number {
    return myTable.length > 0 ? Math.max(...myTable.map((t) => t.id)) + 1 : 11;
  }

  constructor() {}
}
