import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Helm } from '../_models/helm'

@Injectable({
  providedIn: 'root'
})
export class HelmService {

  constructor(private http: HttpClient) { }

  private helmsUrl = 'api/helms';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  getHelms(): Observable<Helm[]> {
    return this.http.get<Helm[]>(this.helmsUrl).pipe(
      tap(_ => this.log('fetched helms')),
      catchError(this.handleError<Helm[]>('getHelms', []))
    );
  }

  getHelm(id: number): Observable<Helm> {
    const url = `${this.helmsUrl}/${id}`;

    return this.http.get<Helm>(url).pipe(
      tap(_ => this.log(`fetched helm id=${id}`)),
      catchError(this.handleError<any>(`getHelm id=${id}`))
    )
  }

  addHelm(helm: Helm): Observable<any> {
    return this.http.post<Helm>(this.helmsUrl, helm, this.httpOptions).pipe(
      tap(_ => this.log('added helm')),
      catchError(this.handleError<Helm>('addHelm'))
    );
  }

  updateHelm(helm: Helm): Observable<any> {
    return this.http.put(this.helmsUrl, helm, this.httpOptions).pipe(
      tap(_ => this.log(`updated helm with id=${helm.id}`)),
      catchError(this.handleError<any>('updateHelm'))
    );
  }

  deleteHelm(helm: Helm | number): Observable<any>{
    const id = typeof helm === 'number' ? helm : helm.id;
    const url = `${this.helmsUrl}/${id}`;

    return this.http.delete<Helm>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted helm id=${id}`)),
      catchError(this.handleError<Helm>('deletedHelm'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(`HelmService: ${message}`);
  }
}
