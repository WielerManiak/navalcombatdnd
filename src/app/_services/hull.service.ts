import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Hull } from '../_models/hull';

@Injectable({
  providedIn: 'root'
})
export class HullService {
  constructor(private http: HttpClient) {}

  private hullsUrl = 'api/hulls';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  getHulls(): Observable<Hull[]> {
    return this.http.get<Hull[]>(this.hullsUrl).pipe(
      tap(_ => this.log('fetched hulls')),
      catchError(this.handleError<Hull[]>('getHulls', []))
    );
  }

  getHull(id: number): Observable<Hull> {
    const url = `${this.hullsUrl}/${id}`;

    return this.http.get<Hull>(url).pipe(
      tap(_ => this.log(`fetched hull id=${id}`)),
      catchError(this.handleError<any>(`getHull id=${id}`))
    )
  }

  addHull(hull: Hull): Observable<any> {
    return this.http.post<Hull>(this.hullsUrl, hull, this.httpOptions).pipe(
      tap(_ => this.log('added hull')),
      catchError(this.handleError<Hull>('addHull'))
    );
  }

  updateHull(hull: Hull): Observable<any> {
    return this.http.put(this.hullsUrl, hull, this.httpOptions).pipe(
      tap(_ => this.log(`updated hull with id=${hull.id}`)),
      catchError(this.handleError<any>('updateHull'))
    );
  }

  deleteHull(hull: Hull | number): Observable<any>{
    const id = typeof hull === 'number' ? hull : hull.id;
    const url = `${this.hullsUrl}/${id}`;

    return this.http.delete<Hull>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted hull id=${id}`)),
      catchError(this.handleError<Hull>('deletedHull'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(`HullService: ${message}`);
  }
}
