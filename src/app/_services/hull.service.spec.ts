import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HullService } from './hull.service';

describe('HullService', () => {
  let service: HullService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
    });
    service = TestBed.inject(HullService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
