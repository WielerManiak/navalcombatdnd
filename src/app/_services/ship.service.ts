import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Ship } from '../_models/ship';

@Injectable({
  providedIn: 'root'
})
export class ShipService {

  constructor(private http: HttpClient) { }

  private shipsUrl = 'api/ships';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  getShips(): Observable<Ship[]> {
    return this.http.get<Ship[]>(this.shipsUrl).pipe(
      tap(_ => this.log('fetched ship')),
      catchError(this.handleError<Ship[]>('getShip', []))
    );
  }

  getShip(id: number): Observable<Ship> {
    const url = `${this.shipsUrl}/${id}`;

    return this.http.get<Ship>(url).pipe(
      tap(_ => this.log(`fetched ship id=${id}`)),
      catchError(this.handleError<any>(`getShip id=${id}`))
    )
  }

  addShip(ship: Ship): Observable<any> {
    return this.http.post<Ship>(this.shipsUrl, ship, this.httpOptions).pipe(
      tap(_ => this.log('added ship')),
      catchError(this.handleError<Ship>('addShip'))
    );
  }

  updateShip(ship: Ship): Observable<any> {
    return this.http.put(this.shipsUrl, ship, this.httpOptions).pipe(
      tap(_ => this.log(`updated ship with id=${ship.id}`)),
      catchError(this.handleError<any>('updateShip'))
    );
  }

  deleteShip(ship: Ship | number): Observable<any>{
    const id = typeof ship === 'number' ? ship : ship.id;
    const url = `${this.shipsUrl}/${id}`;

    return this.http.delete<Ship>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted ship id=${id}`)),
      catchError(this.handleError<Ship>('deletedShip'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(`ShipService: ${message}`);
  }
}
