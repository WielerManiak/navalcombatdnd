import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SailsService } from './sails.service';

describe('SailsService', () => {
  let service: SailsService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])]
    });
    service = TestBed.inject(SailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
