import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Sails } from '../_models/sails'

@Injectable({
  providedIn: 'root'
})
export class SailsService {

  constructor(private http: HttpClient) { }


  private sailssUrl = 'api/sails';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  getSailss(): Observable<Sails[]> {
    return this.http.get<Sails[]>(this.sailssUrl).pipe(
      tap(_ => this.log('fetched sails')),
      catchError(this.handleError<Sails[]>('getSails', []))
    );
  }

  getSails(id: number): Observable<Sails> {
    const url = `${this.sailssUrl}/${id}`;

    return this.http.get<Sails>(url).pipe(
      tap(_ => this.log(`fetched sails id=${id}`)),
      catchError(this.handleError<any>(`getSails id=${id}`))
    )
  }

  addSails(sails: Sails): Observable<any> {
    return this.http.post<Sails>(this.sailssUrl, sails, this.httpOptions).pipe(
      tap(_ => this.log('added sails')),
      catchError(this.handleError<Sails>('addSails'))
    );
  }

  updateSails(sails: Sails): Observable<any> {
    return this.http.put(this.sailssUrl, sails, this.httpOptions).pipe(
      tap(_ => this.log(`updated sails with id=${sails.id}`)),
      catchError(this.handleError<any>('updateSails'))
    );
  }

  deleteSails(sails: Sails | number): Observable<any>{
    const id = typeof sails === 'number' ? sails : sails.id;
    const url = `${this.sailssUrl}/${id}`;

    return this.http.delete<Sails>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted sails id=${id}`)),
      catchError(this.handleError<Sails>('deletedSails'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(`SailsService: ${message}`);
  }
}
