import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HelmService } from './helm.service';

describe('HelmService', () => {
  let service: HelmService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
    });
    service = TestBed.inject(HelmService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
