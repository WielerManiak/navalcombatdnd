import { Component, OnInit } from '@angular/core';
import { Ship}  from '../_models/ship'
import { Board } from '../_models/board'
import { Direction } from '../_models/direction'
import { Weather } from '../_models/weather'
import { Team } from '../_models/team'
import { Cannon } from '../_models/cannon';
import { Position } from '../_models/position';
import { Helm } from '../_models/helm';
import { Hull } from '../_models/hull';
import { Sails } from '../_models/sails';

@Component({
  selector: 'app-ship',
  templateUrl: './ship.component.html',
  styleUrls: ['./ship.component.scss'],
})
export class ShipComponent implements OnInit {

  cannon1 = new Cannon(1,'', "1d20+6", "8d10", 17, 60, 600,2400, 2, Position.Port)
  cannon2 = new Cannon(2,'',"1d20+6", "8d10", 17, 60, 600,2400, 2, Position.Port)

  cannon3 = new Cannon(3,'', "1d20+6", "8d10", 17, 60, 600,2400, 2, Position.Starboard)
  cannon4 = new Cannon(4,'', "1d20+6", "8d10", 17, 60, 600,2400, 2, Position.Starboard)

  helm1 = new Helm(1,"normal helm", 16, 50)
  hull = new Hull(1, "big ship",15 , 1000, 20)
  sails = new Sails(1, "normal sail", 12, 200, 45)

  ship1 = new Ship(1,'ship 1', this.sails, 20, this.hull, this.helm1, 3, 'wind', [this.cannon1, this.cannon2, this.cannon3, this.cannon4], Direction.East)

  ship2 = new Ship(2,'ship 2', this.sails, 20, this.hull, this.helm1, 3, 'wind', [this.cannon1, this.cannon2, this.cannon3, this.cannon4], Direction.West);

  team1 = new Team("Team A", this.ship1, "red")
  team2 = new Team("Team B", this.ship2, "blue")

  board = new Board(100, [this.team1, this.team2], Weather.Clear, Direction.East)

  constructor() {}

  ngOnInit(): void {


  }

  hitHullTest(){
    this.board.teams[0].ship.hitHull(50)
    console.log(this.board.teams[0].ship.hull.actualHitpoints)
  }

  hitHelmTest(){
    this.board.teams[0].ship.hitHelm(10)
    console.log(this.board.teams[0].ship.helm.isDestroyed)
  }

  hitSailsTest(){
    this.board.teams[0].ship.hitSails(50)
    console.log("hit Sail")
  }

  hitCannon1Test(){
    this.board.teams[0].ship.hitCannon(20, 1)
  }

  moveNorthTest(){
    this.board.teams[0].ship.moveNorth(Direction.East)
  }
}
