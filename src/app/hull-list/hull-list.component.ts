import { Component, OnInit } from '@angular/core';
import { Hull } from '../_models/hull';
import { HullService } from '../_services/hull.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-hull-list',
  templateUrl: './hull-list.component.html',
  styleUrls: ['./hull-list.component.scss'],
})
export class HullListComponent implements OnInit {
  hulls: Hull[];
  loading = false;

  constructor(private hullService: HullService, private router: Router) {}

  ngOnInit(): void {
    this.getHulls();
  }

  getHulls(): void {
    this.hullService.getHulls().subscribe((hulls) => (this.hulls = hulls));
  }

  onDelete(id: number): void {
    this.loading = true;
    this.hullService.deleteHull(id).subscribe(() => {
      let index = this.hulls.findIndex((h) => h.id === id);
      this.hulls.splice(index, 1);
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/hull/list']);
      this.loading = false;
    });
  }
}
