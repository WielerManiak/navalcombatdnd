import { Component, OnInit } from '@angular/core';
import { Ship } from '../_models/ship';
import { ShipService } from '../_services/ship.service';
import { Weather } from '../_models/weather';
import { Direction } from '../_models/direction';
import { enumSelector } from '../_helpers/enumSelector';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Board } from '../_models/board';
import { Team } from '../_models/team';
import { Data } from '../_helpers/data';
import { Router } from '@angular/router';
import { Color } from '../_models/color';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss'],
})
export class TeamsComponent implements OnInit {
  ships: Ship[];
  weatherTypes = enumSelector(Weather);
  windDirection = enumSelector(Direction);
  colors = enumSelector(Color);
  teamForm: FormGroup;
  board: Board;
  team1: Team;
  team2: Team;
  loading = false;
  disabledColorTeam2: string;
  disabledColorTeam1: string;

  constructor(
    private shipService: ShipService,
    private data: Data,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.shipService.getShips().subscribe((ship) => (this.ships = ship));

    this.teamForm = new FormGroup({
      weather: new FormControl('', [Validators.required]),
      wind: new FormControl('', [Validators.required]),
      team1: new FormControl('', [Validators.required]),
      team2: new FormControl('', [Validators.required]),
      color1: new FormControl('', [Validators.required]),
      color2: new FormControl('', [Validators.required])
    });
  }

  get f() {
    return this.teamForm.controls;
  }

  onSubmit() {
    console.log(this.teamForm.value);

    if (this.teamForm.invalid) {
      return;
    }

    this.team1 = new Team(
      'Team 1',
      new Ship(
        101,
        this.teamForm.value.team1.name,
        this.teamForm.value.team1.sails,
        this.teamForm.value.team1.crew,
        this.teamForm.value.team1.hull,
        this.teamForm.value.team1.helm,
        this.teamForm.value.team1.size,
        this.teamForm.value.team1.propulsion,
        this.teamForm.value.team1.cannons,
        this.teamForm.value.team1.facing
      ),
      this.teamForm.value.color1
    );
    this.team2 = new Team(
      'Team 2',
      new Ship(
        202,
        this.teamForm.value.team2.name,
        this.teamForm.value.team2.sails,
        this.teamForm.value.team2.crew,
        this.teamForm.value.team2.hull,
        this.teamForm.value.team2.helm,
        this.teamForm.value.team2.size,
        this.teamForm.value.team2.propulsion,
        this.teamForm.value.team2.cannons,
        this.teamForm.value.team2.facing
      ),
      this.teamForm.value.color2
    );

    this.board = new Board(
      60,
      [this.team1, this.team2],
      this.teamForm.value.weather.value,
      this.teamForm.value.wind.value
    );
    this.data.storage = this.board;

    this.router.navigate(['game']);
  }

  selectColorTeam1(color: string){
    let selectedColor = color.split(' ')
    this.disabledColorTeam2 = selectedColor[1];
  }

  selectColorTeam2(color: string){
    let selectedColor = color.split(' ')
    this.disabledColorTeam1 = selectedColor[1];
  }
}
